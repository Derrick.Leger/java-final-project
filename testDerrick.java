import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.layout.HBox;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Button;
import javafx.application.Application;

import javafx.stage.Stage;

public class testDerrick extends Application{
    public static void main(String[] args) 
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        GridPane g=new GridPane();
        Scene scene = new Scene(g,1300,1300);
        Deck d=new Deck();
        d.shuffle();
        Board b=new Board(d);
        
        int rows=b.numRows();
        int columns=b.numCols();
        
        Button exit=new Button("Exit");
        Button newGame=new Button("New Game");
        Button add3=new Button("Add 3");
        
        for(int i=0; i<rows; i++)
        {
            for(int j=0; j<columns; j++)
            {
               CardPane cardpane=new CardPane(b.getBoardSquare(i,j));
               g.add(cardpane,j,i,1,1);
            }
        }
        
        g.add(exit,0,columns,1,1);
        g.add(newGame,1,columns,1,1);
        g.add(add3,2,columns,1,1);
        
        g.setVgap(15);
        g.setHgap(15);

        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
