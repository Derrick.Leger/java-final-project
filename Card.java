public class Card
{  //color
   private final int RED=1;
   private final int PURPLE=2;
   private final int GREEN=3;
   
   //shape
   private final int OVAL=1;
   private final int SQUIGGLE=2;
   private final int DIAMOND=3;
   
   //number
   private final int ONE=1;
   private final int TWO=2;
   private final int THREE=3;
   
   //shading
   private final int SOLID=1;
   private final int STRIPED=2;
   private final int OUTLINED=3;
   
   //charcteristics
   private int color;
   private int shape;
   private int number;
   private int shading;
   /*
   card constructor takes in ints for color, shape, number, shading
   @params color shape number shading
   @returns na
   */
   public Card(int color, int shape, int number, int shading)
   {
      this.color=color;
      this.shape=shape;
      this.number=number;
      this.shading=shading;
   }
   /* isset method takes 3 cards and determines if set
   @params card c1, card c2, card c3
   @returns boolean true or false
   */
   public static boolean isSet(Card c1, Card c2, Card c3)
   {
        if (!(((c1.getNumber()==c2.getNumber())&&(c2.getNumber()==c3.getNumber()))||((c1.getNumber()!=c2.getNumber())&&(c1.getNumber()!=c3.getNumber())&&(c2.getNumber()!=c3.getNumber())))) 
        {
            return false;
        }
        if (!(((c1.getShape()==c2.getShape())&&(c2.getShape()==c3.getShape()))||((c1.getShape()!=c2.getShape())&&(c1.getShape()!=c3.getShape())&&(c2.getShape()!=c3.getShape())))) 
        {
            return false;
        }
        if (!(((c1.getShading()==c2.getShading())&&(c2.getShading()==c3.getShading()))||((c1.getShading()!=c2.getShading())&&(c1.getShading()!=c3.getShading())&&(c2.getShading()!=c3.getShading())))) 
        {
            return false;
        }
        if (!(((c1.getColor()==c2.getColor())&&(c2.getColor()==c3.getColor()))||((c1.getColor()!=c2.getColor())&&(c1.getColor()!=c3.getColor())&&(c2.getColor()!=c3.getColor())))) 
        {
            return false;
        }
      
      return true;
   }
   /*
   getcolor method gets color
   @params none
   @returns this.color
   */
   public int getColor()
   {
      return this.color;
   }
   /* 
   getshape method gets shape
   @params none
   @returns this.shape
   */
   public int getShape()
   {
      return this.shape;
   }
   /*
   getnumber method gets number
   @params none
   @returns this.number
   */
   public int getNumber()
   {
      return this.number;
   }
   /*
   getshading method gets shading
   @params none
   @returns this.shading
   */
   public int getShading()
   {
      return this.shading;
   }
   /* toString describes card object
   @params none
   @returns toString to describe card
   */
   public String toString()
   {
      String color="";
      String shape="";
      String number="";
      String shading="";
      String cardValue="";
      
      if(this.color==1)
      {
         color="RED";
      }
      else if(this.color==2)
      {
         color="PURPLE";
      }
      else if(this.color==3)
      {
         color="GREEN";
      }
      
      if(this.shape==1)
      {
         shape="OVAL";
      }
      else if(this.shape==2)
      {
         shape="SQUIGGLE";
      }
      else if(this.shape==3)
      {
         shape="DIAMOND";
      }
      
      if(this.number==1)
      {
         number="ONE";
      }
      else if(this.number==2)
      {
         number="TWO";
      }
      else if(this.number==3)
      {
         number="THREE";
      }
      
      if(this.shading==1)
      {
         shading="SOLID";
      }
      else if(this.shading==2)
      {
         shading="STRIPED";
      }
      else if(this.shading==3)
      {
         shading="OUTLINED";
      }
      
      cardValue= ""+number+"_"+color+"_"+shape+"_"+shading+" ";
      return cardValue;
   }
}