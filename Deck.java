import java.util.*;
public class Deck
{
   
   ArrayList<Card> Deck=new ArrayList<Card>();
   /* deck constructor constructs deck
   @params none
   @returns deck object
   */
   public Deck()
   {
      for(int i=1; i<4; i++)
      {
         for(int j=1; j<4; j++)
         {
            for(int k=1; k<4; k++)
            {
               for(int l=1; l<4; l++)
               {
                  Card c=new Card(i,j,k,l);
                  Deck.add(c);
               }
            }
         }
      }
   }
   
   
   /* gettopcard method gets top card
   @params none
   @returns topcard
   */
   public Card getTopCard()
   {
      Card topCard;
      topCard=this.Deck.get(0);
      this.Deck.remove(0);
      return topCard;
   }
   
   public int cardsRemaining() 
   {
      return Deck.size();
   }
   
   /* shuffle method shuffles deck
   @params none
   @returns shuffled deck
   */
   public void shuffle()
   {
      Collections.shuffle(Deck);
   }
   
   /* isempty method checks if deck is empty
   @params none
   @returns true or false
   */
   public boolean isEmpty()
   {
      if(this.Deck.size()==0)
      {
         return true;
      }
      else
      {
         return false;
      }
   }
   
   /* tostring describes deck
   @params none
   @returns deckStr
   */
   public String toString()
   {
      String deckStr="";
      for(int i=0; i<Deck.size(); i++)
      {
         deckStr+=Deck.get(i).toString()+"\n";
      }
      return deckStr;
   }
}