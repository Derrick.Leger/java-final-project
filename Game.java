import java.util.*;

public class Game
{  
   private Board board;
   private BoardSquare boardsSquare;
   private Deck deck;
   private Card card;
   private ArrayList<BoardSquare> selected=new ArrayList<BoardSquare>();
   
   /* game constructor
   @params none
   @returns none
   */
   public Game()
   {
      this.deck=new Deck();
      this.deck.shuffle();
      this.board=new Board(deck);
   }
   /* out of cards method checks if deck is out of cards
   @params none
   @returns boolean
   */
   public boolean outOfCards()
   {
      if(this.deck.isEmpty()==true)
      {
         return true;
      }
      else
      {
         return false;
      }
   }
   /* cards remaining method
   @returns numcards remaining in deck
   @params none*/
   public int cardsRemaining() 
   {
        return deck.cardsRemaining();
   }
   
   /* getBoard method returns board object
   @params none
   @returns board*/
   public Board getBoard() 
   {
        return board;
   }
    
   /* addtoselected method
   @params int row,column
   @returns none
   */
   public void addToSelected(int row, int column)
   {
      BoardSquare boardSquare=board.getBoardSquare(row,column);
      this.selected.add(boardSquare);
   }
   /* numselected method counts amount of cards selected
   @params none
   @returns amouint of selected cards
   */
   public int numSelected()
   {
      return selected.size();
   }
   /* testselected method tests if cards are a set
   @params none
   @returns none
   */
   public boolean testSelected()
   {
      Card c1=selected.get(0).getCard();
      Card c2=selected.get(1).getCard();
      Card c3=selected.get(2).getCard();
      
      if(card.isSet(c1,c2,c3))
      {
         boolean set=true;
         System.out.println("set");
         this.board.replaceCard(this.deck.getTopCard(), this.selected.get(0).getRow(), this.selected.get(0).getColumn());
         this.board.replaceCard(this.deck.getTopCard(), this.selected.get(1).getRow(), this.selected.get(1).getColumn());
         this.board.replaceCard(this.deck.getTopCard(), this.selected.get(2).getRow(), this.selected.get(2).getColumn());
         for(BoardSquare b: selected)
         {
            b.setSelected(false);
         }
         this.selected.removeAll(this.selected);
         return set;
      }
      else
      {
         boolean set=false;
         System.out.println("Not a set");
         for(BoardSquare b: selected)
         {
            b.setSelected(false);
         }
         this.selected.removeAll(this.selected);
         return set;
      }
   }
   /* remove selected method removes selected cards
   @params int row,column
   @returns none */
   public void removeSelected(int row, int column)
   {      
      //boolean status=false;
     selected.remove(board.getBoardSquare(row,column));
   }
   
   /* add3 method adds 3 cards to the board
   @params deck
   @returns none
   */
   public void add3()
   {
      this.board.add3(this.deck);
      
   }
   /* getselected method gets selected cards
   @params none
   @returns none
   */
   public ArrayList<BoardSquare> getSelected()
   {
      return this.selected;
   }
   /* tostring method returns string representation of board object
   @params none
   @returns none
   */
   public String toString()
   {
      return this.board.toString();
   }
}