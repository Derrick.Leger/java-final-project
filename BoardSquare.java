public class BoardSquare
{
   private int row;
   private int column;
   private Card card;
   private boolean selected=false;
   /* boardSquare constructor
   @params card row column
   @returns none
   */
   public BoardSquare(Card card, int row, int column)
   {
      this.card=card;
      this.row=row;
      this.column=column;
   }   
   /*
   setrow method sets row
   @params int row
   @returns none
   */
   public void setRow(int row)
   {
      this.row=row;
   }
   /*getrow method gets row
   @params none
   @returns this.row
   */
   public int getRow()
   {
      return this.row;
   }
   /* setcolumn method sets column
   @params int column
   @returns none
   */
   public void setColumn(int column)
   {
      this.column=column;
   }
   /* getcolumn method gets method
   @params none
   @returns column
   */
   public int getColumn()
   {
      return column;
   }
   /* setCard method sets card
   @params card card
   @returns none
   */
   public void setCard(Card card)
   {
      this.card=card;
   }
   /* getCard method gets card
   @params none
   @returns this.card
   */
   public Card getCard()
   {
      return this.card;
   }
   
   public boolean isSelected()
   {
      return selected;
   }
   
   public void setSelected(boolean s)
   {
      selected = s;
   }
   
   /* tostring
   @params none
   @returns string interpretation of board square */ 
   public String toString()
   {
      return this.getCard().toString();
   }
}