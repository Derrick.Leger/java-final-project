import javafx.scene.input.MouseEvent;
import javafx.application.Application;
import javafx.scene.layout.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.text.TextFlow;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class gameGUI extends Application
{
    private Game game;
    private Stage stage;
    private Scene scene;
    private BorderPane layout;
    private AnchorPane cardsRemainingPane;
    private AnchorPane menuPane;
    private HBox hBoxmenu;
    private GridPane grid;
    private Label cardsRemaining;
    private Label status;
    
   /* start method 
   @params stage primary stage
   @returns none */
   @Override
   public void start(Stage primaryStage)
   {
      game=new Game();
      cardsRemainingPane = new AnchorPane();
      this.stage = primaryStage;
      stage.setTitle("Game of Set");
      layout = new BorderPane();
      
      TextFlow titlecss = new TextFlow();
      titlecss.setTextAlignment(TextAlignment.CENTER);
      titlecss.setLineSpacing(1.0);

      Text title = new Text("Game of Set");
      title.setFont(Font.font("Arial", 36));

      Text author = new Text("\n  By: Derrick Leger");
      author.setFill(Color.TEAL);
      author.setFont(Font.font("Arial", 12));
      titlecss.getChildren().add(title);
      titlecss.getChildren().add(author);
      
      String cardsRemainingLabel=String.format("Cards remaining: "+ game.cardsRemaining());
      cardsRemaining = new Label(cardsRemainingLabel);
      cardsRemaining.setFont(Font.font("Arial",12));
      
      AnchorPane.setTopAnchor(cardsRemaining,10.0);
      AnchorPane.setRightAnchor(cardsRemaining,10.0);
            
      cardsRemainingPane.getChildren().add(titlecss);
      cardsRemainingPane.getChildren().add(cardsRemaining);
      
      menuPane = new AnchorPane();
      status = new Label("");
      status.setFont(Font.font("Arial", 15));
      AnchorPane.setRightAnchor(status,20.0);
      AnchorPane.setBottomAnchor(status,3.0);
      AnchorPane.setTopAnchor(status,3.0);
      menuPane.getChildren().add(status);
      
      hBoxmenu = new HBox(10);
      hBoxmenu.setPadding(new Insets(1,5,1,5));
      hBoxmenu.setAlignment(Pos.CENTER_RIGHT);
      AnchorPane.setLeftAnchor(hBoxmenu,3.0);   
      AnchorPane.setBottomAnchor(hBoxmenu,3.0);   
      AnchorPane.setTopAnchor(hBoxmenu,3.0);
      menuPane.getChildren().add(hBoxmenu);
      
      //buttons, beginning of gui
      Button exitBtn = new Button("Exit");
      exitBtn.setOnAction(this::exit);
      hBoxmenu.getChildren().add(exitBtn);
      
      Button add3Btn = new Button("Add 3 Cards");
      add3Btn.setOnAction(this::add3);
      hBoxmenu.getChildren().add(add3Btn);

      Button newGameBtn = new Button("New Game");
      newGameBtn.setOnAction(this::newGame);
      hBoxmenu.getChildren().add(newGameBtn);
        
      
      grid=new GridPane();
      grid.setVgap(5);
      grid.setHgap(5);
      grid.setPadding(new Insets(5));
      
      layout.setCenter(grid);
      layout.setTop(cardsRemainingPane);
      layout.setBottom(menuPane);
      
      this.drawBoard();
      
      scene = new Scene(layout);
      stage.setScene(scene);
      stage.show();

      stage.sizeToScene();
      
      stage.setMinWidth(stage.getWidth());
      stage.setMinHeight(stage.getHeight());
   }
   
   /*draw board draws a new game board
   @paarams none
   @returns none */
   public void drawBoard()
   {
      //erases board initially then creates new board
      grid.getChildren().clear();
      
      Board b = game.getBoard();
      for (int i=0; i<b.numRows(); i++) 
      {
         for (int j=0; j<b.numCols(); j++) 
         {
            Pane cardpane = new CardPane(b.getBoardSquare(i,j));
            cardpane.setOnMouseClicked(this::selectCardPane);
            grid.add(cardpane,j,i);
         }
         
         if (b.numCols()>=4)
         {
            stage.sizeToScene();
         }
         
         
      }//end for loop
      
      
   }//end drawboard
   
   /* action for exit button
   @params action event e
   @returns none */
   public void exit(ActionEvent e) 
   { 
      Platform.exit();
   }
   /* creates new game for new game button
   @params action event e
   @returns none*/
   public void newGame(ActionEvent e)
   {
      game = new Game();
      this.drawBoard();
      String cardsRemainingLabel=String.format("Cards remaining: %d", game.cardsRemaining());
      cardsRemaining.setText(String.format(cardsRemainingLabel));      status.setText("");
      System.gc();//garbage collection method
   }
    /*adds 3 cards for add3 button
    @params action event e
    @returns none */
   public void add3(ActionEvent e)
   {
      if (game.cardsRemaining()>=3 && grid.getChildren().size()<18) 
      {
            game.add3();
            this.drawBoard();
            String cardsRemainingLabel=String.format("Cards remaining: %d", game.cardsRemaining());
            cardsRemaining.setText(String.format(cardsRemainingLabel));      }
      else
      {
         if (grid.getChildren().size()>=18)
         {
            status.setText("Max Cards On Board");
         }
         else if (game.cardsRemaining()<3)
         {
            status.setText("Not Enough Cards On Board");
         }
      }
   }
   /* select cardpane method for selecting cardpane
   @params mouse event e
   @returns none
   */
   public void selectCardPane(MouseEvent e)
   {
      CardPane cardpane=(CardPane)e.getSource();
      BoardSquare bs=cardpane.getBoardSquare();
      
      if (!bs.isSelected()) 
      {
         bs.setSelected(true);
         cardpane.setStyle("-fx-background-color: yellow;"
         + "-fx-border-width: 2;"
         + "-fx-border-color: white;"
         + "-fx-border-style: solid;");
         game.addToSelected(cardpane.getRow(),cardpane.getColumn());
      }
      else if (bs.isSelected()) 
      {
         bs.setSelected(false);
         cardpane.setStyle("-fx-background-color: transparent;"
         + "-fx-border-width: 5;"
         + "-fx-border-color: black;"
         + "-fx-border-style: solid;");
         game.removeSelected(cardpane.getRow(),cardpane.getColumn());
        
      }
      
      if (game.numSelected()==3) 
      {
         boolean isSet=game.testSelected();

         if(isSet)
         {
            System.out.println("set found");
            
         }
         else
         {
            System.out.println("not a set");
         }
         game.getSelected().clear();
         this.drawBoard();
         
          

         // update cards remaining label
         String cardsRemainingLabel=String.format("Cards remaining: %d", game.cardsRemaining());
         cardsRemaining.setText(String.format(cardsRemainingLabel));
       }
   }
   
   public static void main(String[] args) 
   {
        launch(args);
   }
   
}//end class gameGUI