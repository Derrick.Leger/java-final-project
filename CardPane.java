import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import java.io.FileInputStream;

public class CardPane extends VBox
{
    private BoardSquare boardSquare;
    private int row;
    private int column;
    private boolean selected;
    private Paint line;
    private Paint fill;
    private int numShapes;
    private static Image image;
    private static ImagePattern hatchPattern;
    private static boolean imageFound;
    public static final int WIDTH = 200;
    public static final int HEIGHT = 250;
    
   /* cardpane constructor
   @params boardsquare b
   @returns none */
   public CardPane(BoardSquare b)
   {
      super();
      this.setAlignment(Pos.CENTER);
      this.line=Color.PINK;
      this.setPrefSize(250,200);
      setHeight(HEIGHT);
      setMaxHeight(HEIGHT);
      setMinHeight(HEIGHT);
      setWidth(WIDTH);
      setMaxWidth(WIDTH);
      setMinWidth(WIDTH);
      this.setStyle("-fx-border-width: 5;"
                        +"-fx-border-color: black;");         
         try{
               image = new Image("hatch.png");
               hatchPattern = new ImagePattern(image);
               imageFound = true;
            } 
            catch (IllegalArgumentException e) 
            {
               imageFound = false;
            }
            
        boardSquare = b;

        row = b.getRow();
        column = b.getColumn();
        selected = b.isSelected();
            
        //call getcard from boardsquare
        Card c=b.getCard();
        
        if(c.getColor()==1)
        {
            this.line=Color.RED;
        }
        else if(c.getColor()==2)
        {
            this.line=Color.PURPLE;
        }
        else if(c.getColor()==3)
        {
            this.line=Color.GREEN;
        }
        
        if(c.getNumber()==1)
        {
            this.numShapes=1;
        }
        else if(c.getNumber()==2)
        {
            this.numShapes=2;
        }
        else if(c.getNumber()==3)
        {
            this.numShapes=3;
        }
        
        if(c.getShading()==1)
        {
            this.fill=line;
        }
        else if(c.getShading()==2)
        {
            this.fill = hatchPattern;
        }
        else if(c.getShading()==3)
        {
            this.fill=Color.WHITE;
        }
        
        if(c.getShape()==1)
        {
            drawEllipse(fill,image);
        }
        else if(c.getShape()==2)
        {
            drawSquiggle(fill,image);
        }
        else if(c.getShape()==3)
        {
            drawDiamond(fill,image);
        }
   }   
   /* getboardsquare method gets boardsquare object
   @params none
   @returns boardsquare */
   public BoardSquare getBoardSquare() 
    {
        return boardSquare;
    }
    /* getrow method gets row
    @params none
    @returns row */
    public int getRow() 
    {
        return row;
    }
    
    /* getcolumn method gets column
    @params none
    @returns column */
    public int getColumn() 
    {
        return column;
    }
    
    /* isselected method returns selected boolean
    @params none
    @returns boolean selected */
    public boolean isSelected() 
    {
        return selected;
    }
    
    /* draw ellipse method draws ellipse
    @params paint, fill
    @returns none */
   public void drawEllipse(Paint fill, Image image)
   {
      Ellipse ellipse;
      Rectangle r;
      for (int i=0;i<this.numShapes;i++) 
      {
         ellipse = new Ellipse(); 
         ellipse.setRadiusX(40.0);
         ellipse.setRadiusY(25.0);
         ellipse.setStroke(line);
         ellipse.setFill(fill);
         getChildren().add(ellipse);
         
         r = new Rectangle();
         r.setWidth(125);
         r.setHeight(5);
         r.setFill(Color.TRANSPARENT);
         r.setStroke(Color.TRANSPARENT);
         getChildren().add(r);
         
      }
   }
   
   /* draw squiggle method draws squiggle
    @params paint, fill
    @returns none */
   public void drawSquiggle(Paint fill, Image image)
   {
   //set the drawing of the Squiggle to be the getters
   //defined above.
      Rectangle r,r2;
      for (int i=0;i<this.numShapes;i++) 
      {
         r = new Rectangle();
         r.setWidth(125);
         r.setHeight(63);
         r.setFill(fill);
         r.setStroke(this.line);
         getChildren().add(r);
         
         r2 = new Rectangle();
         r2.setWidth(125);
         r2.setHeight(7);
         r2.setFill(Color.TRANSPARENT);
         r2.setStroke(Color.TRANSPARENT);
         getChildren().add(r2);
         
      }
   }
   
   /* draw diamond method draws diamond
    @params paint, fill
    @returns none */
   public void drawDiamond(Paint fill, Image image)
   {
   //set the drawing of the Diamond to be the getters
   //defined above.
      Rectangle r;
      Polygon polygon;
      for (int i=0;i<this.numShapes;i++)
      {
          polygon=new Polygon();
          polygon.getPoints().addAll(new Double[]{
          100.0, 25.0,
          175.0,50.0,
          100.0, 75.0,
          25.0, 50.0,
          });
         polygon.setFill(fill);
         polygon.setStroke(line);
         getChildren().add(polygon);
         
         r = new Rectangle();
         r.setWidth(125);
         r.setHeight(5);
         r.setFill(Color.TRANSPARENT);
         r.setStroke(Color.TRANSPARENT);
         getChildren().add(r);
      }
   }

   public static void main(String args [])
   {
      
   }

}